# recipe-app-api-proxy
NGINX proxy app for our recipe App API

Usage:
List of the Config Items such as Environment variables to be used to configure our Nginx proxy

LISTEN_PORT - The port the Nginx listens on for incoming connections (Default 800)

APP_HOST - This is the hostname of the app to forwards requests to 

APP_PORT - The port of the app to forward Nginx requests to (Default 9000)
